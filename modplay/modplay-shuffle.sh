#!/bin/bash

files=(*/*)
mapfile -t files < <(shuf -e "${files[@]}")

for i in "${files[@]}"
do
  artist=$(dirname "$i")
  file=$(realpath "$i")
  printf "\n%s\n" "$artist"
  modplay.sh "$artist" "$file"
done
