// 2025 Artem Lukyanov
// Docs:
// https://github.com/NollieL/SignalRgb_CN_Key

#include <errno.h>
#include <fcntl.h>
#include <linux/hidraw.h>
#include <linux/input.h>
#include <pthread.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#define HID_VENDOR_ID 0x258a
#define HID_PRODUCT_ID 0x010c
#define VKEYS_LENGTH sizeof(vKeys) / sizeof(vKeys[0])
#define BUFFER_SIZE 520
#define FIFO_PATH "/var/tmp/aula-f87-rgb"

const int vKeys[] = {0,  12, 18, 24, 30, 36, 42, 48, 54, 60, 66, 72, 78, 84, 90,
                     96, 1,  7,  13, 19, 25, 31, 37, 43, 49, 55, 61, 67, 73, 79,
                     85, 91, 97, 2,  8,  14, 20, 26, 32, 38, 44, 50, 56, 62, 68,
                     74, 80, 86, 92, 98, 3,  9,  15, 21, 27, 33, 39, 45, 51, 57,
                     63, 69, 81, 4,  10, 16, 22, 28, 34, 40, 46, 52, 58, 64, 82,
                     94, 5,  11, 17, 35, 53, 59, 65, 83, 89, 95, 101};

struct report_fields {
  uint8_t length : 2;
  uint8_t type : 2;
  uint8_t function : 4;
};
typedef union report {
  uint8_t value;
  struct report_fields fields;
} report;

char buf[BUFFER_SIZE];

#define REPORT_USAGE_PAGE(x) x.fields.type == 0x01 && x.fields.function == 0x00
#define REPORT_USAGE(x) x.fields.type == 0x02 && x.fields.function == 0x00

void *reader_process(void *vargp) {
  int i, iLedIdx, res, fd, key = -1, r = 0, g = 0, b = 0;
  char *pch, buffer[11];

  while (1) {
    fd = open(FIFO_PATH, O_RDONLY);
    if (fd == -1) {
      perror("open");
      exit(EXIT_FAILURE);
    }

    res = read(fd, buffer, sizeof(buffer));
    if (res == -1) {
      perror("read");
      exit(EXIT_FAILURE);
    }
    pch = strtok(buffer, " \n");
    if (pch == NULL) {
      goto fd;
    }
    key = strtol(pch, NULL, 10);
    pch = strtok(NULL, " \n");
    if (pch == NULL) {
      goto fd;
    }
    sscanf(pch, "%02x%02x%02x", &r, &g, &b);

    if (key > 0 && key <= VKEYS_LENGTH) {
      iLedIdx = vKeys[key - 1] * 3;
      buf[iLedIdx + 8] = r;
      buf[iLedIdx + 1 + 8] = g;
      buf[iLedIdx + 2 + 8] = b;
    } else if (key == 255) {
      for (i = 0; i < VKEYS_LENGTH; i++) {
        iLedIdx = vKeys[i] * 3;
        buf[iLedIdx + 8] = r;
        buf[iLedIdx + 1 + 8] = g;
        buf[iLedIdx + 2 + 8] = b;
      }
    }

  fd:
    close(fd);
  }
}

int main(int argc, char *argv[]) {
  int fd, res, desc_size, i;
  report rpt_fields;
  struct hidraw_devinfo info;
  struct hidraw_report_descriptor rpt_desc;
  pthread_t tid;

  if (argc != 2) {
    printf("path to hidraw device not given\n");
    return 1;
  }

  fd = open(argv[1], O_RDWR | O_NONBLOCK);
  if (fd < 0) {
    perror("Unable to open device");
    return 1;
  }

  res = ioctl(fd, HIDIOCGRAWINFO, &info);
  if (res < 0) {
    perror("HIDIOCGRAWINFO");
    return 1;
  }
  if (info.vendor != HID_VENDOR_ID || info.product != HID_PRODUCT_ID) {
    printf("wrong device\n");
    return 1;
  }

  desc_size = 0;
  res = ioctl(fd, HIDIOCGRDESCSIZE, &desc_size);
  if (res < 0) {
    perror("HIDIOCGRDESCSIZE");
    return 1;
  }

  memset(&rpt_desc, 0x0, sizeof(rpt_desc));
  rpt_desc.size = desc_size;
  res = ioctl(fd, HIDIOCGRDESC, &rpt_desc);
  if (res < 0) {
    perror("HIDIOCGRDESC");
    return 1;
  }

  i = 0, res = -1;
  while (i < rpt_desc.size) {
    rpt_fields.value = rpt_desc.value[i];
    if (REPORT_USAGE_PAGE(rpt_fields) && rpt_fields.fields.length == 2 &&
        rpt_desc.value[i + 1] == 0x00 && rpt_desc.value[i + 2] == 0xff) {
      i = i + 1 + rpt_fields.fields.length;
      break;
    }
    i = i + 1 + rpt_fields.fields.length;
  }
  while (i < rpt_desc.size) {
    rpt_fields.value = rpt_desc.value[i];
    if (REPORT_USAGE(rpt_fields) && rpt_fields.fields.length == 1 &&
        rpt_desc.value[i + 1] == 0x01) {
      res = 0;
      break;
    }
    i = i + 1 + rpt_fields.fields.length;
  }
  if (res < 0) {
    printf("wrong device\n");
    return 1;
  }

  memset(buf, 0x0, BUFFER_SIZE);
  buf[0] = 0x06;
  buf[1] = 0x08;
  buf[2] = 0x00;
  buf[3] = 0x00;
  buf[4] = 0x01;
  buf[5] = 0x00;
  buf[6] = 0x7a;
  buf[7] = 0x01;

  umask(0);
  mkfifo(FIFO_PATH, S_IWUSR | S_IWGRP | S_IWOTH);

  pthread_create(&tid, NULL, reader_process, NULL);

  while (1) {
    res = ioctl(fd, HIDIOCSFEATURE(BUFFER_SIZE), buf);
    if (res < 0) {
      perror("HIDIOCSFEATURE");
      return 1;
    }
    usleep(1000);
  }

  return 1;
}
